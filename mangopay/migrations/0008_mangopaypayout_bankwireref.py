# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mangopay', '0007_auto_20160404_1021'),
    ]

    operations = [
        migrations.AddField(
            model_name='mangopaypayout',
            name='bankwireref',
            field=models.CharField(max_length=12, null=True, blank=True),
        ),
    ]
