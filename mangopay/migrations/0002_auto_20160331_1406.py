# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mangopay', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mangopaylegaluser',
            name='headquaters_address',
        ),
        migrations.RemoveField(
            model_name='mangopayuser',
            name='address',
        ),
    ]
