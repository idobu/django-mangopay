# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mangopay', '0006_auto_20160404_0840'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mangopaylegaluser',
            name='headquaters_country',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
