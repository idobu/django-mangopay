# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mangopay', '0003_auto_20160331_1406'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mangopaybankaccount',
            name='address',
        ),
        migrations.AddField(
            model_name='mangopaybankaccount',
            name='owner_address',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mangopaybankaccount',
            name='owner_city',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mangopaybankaccount',
            name='owner_country',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mangopaybankaccount',
            name='owner_postalcode',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mangopaybankaccount',
            name='owner_region',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
    ]
