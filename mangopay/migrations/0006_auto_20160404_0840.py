# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mangopay', '0005_auto_20160404_0829'),
    ]

    operations = [
        migrations.RenameField(
            model_name='mangopayuser',
            old_name='address_vat',
            new_name='address_country',
        ),
    ]
