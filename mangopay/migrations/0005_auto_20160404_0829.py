# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_countries.fields


class Migration(migrations.Migration):

    dependencies = [
        ('mangopay', '0004_auto_20160402_1112'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mangopaylegaluser',
            name='headquaters_address',
        ),
        migrations.RemoveField(
            model_name='mangopayuser',
            name='address',
        ),
        migrations.AddField(
            model_name='mangopaylegaluser',
            name='headquaters_city',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mangopaylegaluser',
            name='headquaters_country',
            field=django_countries.fields.CountryField(blank=True, max_length=2, null=True),
        ),
        migrations.AddField(
            model_name='mangopaylegaluser',
            name='headquaters_postcode',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mangopaylegaluser',
            name='headquaters_region',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mangopaylegaluser',
            name='headquaters_street',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mangopayuser',
            name='address_city',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mangopayuser',
            name='address_postcode',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mangopayuser',
            name='address_region',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mangopayuser',
            name='address_street',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mangopayuser',
            name='address_vat',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
