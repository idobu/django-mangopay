# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import address.models


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0001_initial'),
        ('mangopay', '0002_auto_20160331_1406'),
    ]

    operations = [
        migrations.AddField(
            model_name='mangopaylegaluser',
            name='headquaters_address',
            field=address.models.AddressField(blank=True, to='address.Address', null=True),
        ),
        migrations.AddField(
            model_name='mangopayuser',
            name='address',
            field=address.models.AddressField(blank=True, to='address.Address', null=True),
        ),
    ]
