from django.contrib import admin
from copy import deepcopy
from django.contrib.auth.models import User
from .models import *
from django import forms
from .tasks import actions
from django_filepicker.widgets import *


class MangoPayAdmin(admin.ModelAdmin):
    actions = ['create_on_mangopay']

    def create_on_mangopay(self, request, queryset):
        for item in queryset:
            _class = type(item).__name__
            if actions['create'].has_key(_class):
                print "starting"
                actions['create'][_class].delay(item.id)
    create_on_mangopay.short_description = "Create item in MangoPay"

    def get_actions(self, request):
        actions = super(MangoPayAdmin, self).get_actions(request)
        return actions

class MangopayNaturalUserAdmin(MangoPayAdmin):
    list_display = ['label', 'seats', 'conference', 'location']

class MangopayBankAccountAdmin(MangoPayAdmin):
    list_display = ['mangopay_id', 'mangopay_user', 'iban', 'owner_name']

class MangopayLegalUserAdmin(MangoPayAdmin):
    list_display = ['label', 'seats', 'conference', 'location']

class MangopayWalletAdmin(MangoPayAdmin):
    list_display = ['mangopay_id', 'mangopay_user', 'currency']

class MangopayPayInWebAdmin(MangoPayAdmin):
    list_display = ['mangopay_id', 'mangopay_user', 'mangopay_wallet', 'execution_date', 'debited_funds','fees', 'status']


class PageAdminForm(forms.ModelForm):
    class Meta:
        model = MangoPayPage
        exclude =[]
        widgets = {
          'file':FPFileWidget
        }

class MangoPayPageInlineAdmin(admin.StackedInline):
    model = MangoPayPage
    form = PageAdminForm
    

class MangoPayDocumentAdmin(MangoPayAdmin):
    list_display = ['mangopay_id', 'mangopay_user', 'type', 'status']
    list_filter = ['mangopay_user',]
    inlines = [MangoPayPageInlineAdmin,]


admin.site.register(MangoPayUser, MangoPayAdmin)
admin.site.register(MangoPayNaturalUser, MangoPayAdmin)
admin.site.register(MangoPayLegalUser, MangoPayAdmin)
admin.site.register(MangoPayDocument, MangoPayDocumentAdmin)
admin.site.register(MangoPayBankAccount, MangopayBankAccountAdmin)
admin.site.register(MangoPayWallet, MangopayWalletAdmin)
admin.site.register(MangoPayPayInWeb, MangopayPayInWebAdmin)
admin.site.register(MangoPayPayInBankWire, MangoPayAdmin)
admin.site.register(MangoPayPayOut, MangoPayAdmin)
admin.site.register(MangoPayCard, MangoPayAdmin)
admin.site.register(MangoPayCardRegistration, MangoPayAdmin)
admin.site.register(MangoPayPayIn, MangoPayAdmin)
admin.site.register(MangoPayRefund, MangoPayAdmin)
admin.site.register(MangoPayTransfer, MangoPayAdmin)

